<?php

/**
 * @file
 * Enables the option to handle no results behaviour of blocks.
 */

define('BLOCK_NORESULTS_NONE', 0);

/**
 * Implements hook_help().
 */
function block_noresults_help($path, $arg) {
  $help_text = '';
  switch ($path) {
    case 'admin/help#block_noresults':
      $help_text = '<p>' . t('Block No results module provides the option to set the displays the custom text on the no results behaviour.') . '</p>';
      break;
  }
  return $help_text;
}

/**
 * Implements hook_form_FORMID_alter().
 *
 * Adds no-results specific visibility options to block configuration form.
 *
 * @see block_add_block_form()
 */
function block_noresults_form_block_add_block_form_alter(&$form, &$form_state, $form_id) {
  block_noresults_form_block_admin_configure_alter($form, $form_state, $form_id);
}

/**
 * Implements hook_form_FORMID_alter().
 *
 * Adds no-results specific visibility options to block configuration form.
 *
 * @see block_admin_configure()
 */
function block_noresults_form_block_admin_configure_alter(&$form, &$form_state, $form_id) {

  $module = $form['module']['#value'];
  $delta = $form['delta']['#value'];
  $form['visibility']['block_noresults'] = array(
    '#type' => 'fieldset',
    '#title' => t('No Results Behaviour'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'visibility',
    '#weight' => 100,
  );

  $description = t('Specify the text when there will be No Results for this block.');
  $bnr = block_noresults_load($module, $delta);
  // Blocks created by views module should use its own No results Behaviour.
  if ($module == 'views') {
    $name = substr($delta, 0, strpos($delta, '-'));
    $description = t('Its better to use the <a href="@views_url">Views No results behaviour</a>.',
      array('@views_url' => url('admin/structure/views/view/' . $name)));
  }
  $form['visibility']['block_noresults']['block_noresults_bnrid'] = array(
    '#type' => 'value',
    '#value' => isset($bnr->bnrid) ? $bnr->bnrid : '',
  );
  $form['visibility']['block_noresults']['block_noresults_status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enables the No Results behaviour for this block.'),
    '#default_value' => isset($bnr->status) ? $bnr->status : BLOCK_NORESULTS_NONE,
  );
  $form['visibility']['block_noresults']['block_noresults_empty_criteria'] = array(
    '#type' => 'select',
    '#title' => t('Block No Results Empty Criteria'),
    '#options' => array(
      'content' => t('Body'),
      'both' => t('Title and Body'),
    ),
    '#default_value' => isset($bnr->empty_criteria) ? $bnr->empty_criteria : 'both',
    '#description' => t('Choose <em>Body</em> if you want to display no results text if block body is empty.<br/> <em>Title and Body</em> if you want to display no results text if block title and body is empty.'),
  );
  $form['visibility']['block_noresults']['block_noresults_content'] = array(
    '#type' => 'text_format',
    '#title' => t('Block No Results Content'),
    '#default_value' => isset($bnr->content) ? $bnr->content : '',
    '#format' => isset($bnr->format) ? $bnr->format : NULL,
    '#description' => $description,
  );
  $form['#submit'][] = 'block_noresults_form_block_admin_configure_submit';
}

/**
 * Block form submit. Saves/Update blocks.
 */
function block_noresults_form_block_admin_configure_submit($form, &$form_state) {

  $values = $form_state['values'];
  $bnr = new stdClass();
  $bnr->module = $values['module'];
  $bnr->delta = $values['delta'];
  $bnr->status = $values['block_noresults_status'];
  $bnr->empty_criteria = $values['block_noresults_empty_criteria'];
  $bnr->content = $values['block_noresults_content']['value'];
  $bnr->format = $values['block_noresults_content']['format'];
  $bnr->bnrid = $values['block_noresults_bnrid'];
  if ($bnr->bnrid) {
    $return = drupal_write_record('block_noresults', $bnr, 'bnrid');
  }
  else {
    $return = drupal_write_record('block_noresults', $bnr);
  }
  return $return;
}

/**
 * Implements hook_form_FORMID_alter().
 *
 * Adds node specific submit handler to delete custom block form.
 *
 * @see block_custom_block_delete()
 */
function block_noresults_form_block_custom_block_delete_alter(&$form, &$form_state) {
  array_unshift($form['#submit'], 'block_noresults_form_block_custom_block_delete_submit');
}

/**
 * Form submit handler for custom block delete form.
 *
 * @see node_form_block_custom_block_delete_alter()
 */
function block_noresults_form_block_custom_block_delete_submit($form, &$form_state) {

  $block = block_load('block', $form_state['values']['bid']);
  db_delete('block_noresults')
    ->condition('module', $block->module)
    ->condition('delta', $block->delta)
    ->execute();
}

/**
 * Implements hook_block_view_alter().
 */
function block_noresults_block_view_alter(&$data, $block) {

  $data = isset($data) ? array_filter($data) : $data;
  $bnr = block_noresults_load($block->module, $block->delta);
  // Handles for those blocks which have no results data.
  if (is_object($bnr)) {
    switch ($bnr->empty_criteria) {

      case 'content':
        if (!isset($data['content'])) {
          if ($bnr->status) {
            $data['content'] = check_markup($bnr->content, $bnr->format, '', TRUE);
          }
        }
        break;

      case 'both':
        if (!isset($data['subject']) && !isset($data['content'])) {
          if ($bnr->status) {
            $data['content'] = check_markup($bnr->content, $bnr->format, '', TRUE);
          }
        }
        break;
    }
  }
}

/**
 * Loads a block noresults object from the database.
 *
 * @param string $module
 *   Name of the module that implements the block to load.
 * @param string $delta
 *   Unique ID of the block within the context of $module. Pass NULL to return
 *   an empty block object for $module.
 *
 * @return object
 *   A block noresults object.
 */
function block_noresults_load($module, $delta) {

  $bnr = db_select('block_noresults', 'bnr')
    ->fields('bnr')
    ->condition('module', $module, '=')
    ->condition('delta', $delta, '=')
    ->execute()
    ->fetchObject();
  return $bnr;
}
