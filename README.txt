CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Contact

INTRODUCTION
------------
There could be requirement of something like Views No Results Behaviour for
blocks. Then there is a need to set "No Results behaviour" for the custom blocks
and modules provided blocks.

The Block No Results module provides option in blocks to specify the text when
the certain block does not have any results.

REQUIREMENTS
-------------
This module has dependancy of block module, that need to be installed in your drupal site.

INSTALLATION
------------
* Install as usual,
see https://www.drupal.org/documentation/install/modules-themes/modules-7.

CONFIGURATION
-------------
* Configure any Block Home » Administration » Structure » Blocks
* Go to Configure page and specify the no results behaviour.

CONTACT
-------
Current maintainers:
  * Naveen Valecha (naveenvalecha) - http://drupal.org/u/naveenvalecha
